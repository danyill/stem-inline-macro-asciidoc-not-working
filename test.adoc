= Just demonstrating that stem macros aren't working correctly
:stem: latexmath
:eqnums: all
:data-uri:

Defining the terms in brackets we are left with:

[latexmath]
++++
I_N = K_1 I_1 + K_2 I_2  
++++

We can observe that stem:[K_1] and stem:[K_2] are related. stem:[K_A], stem:[K_B], stem:[K_C] are all real terms (only related to capacitive reactances) and stem:[a^2=a^\text{*}].

We are left with:

[latexmath]
++++
I_N = K_1 I_1  + K_1^* I_2  
++++

This definition allows us to say that if we know the positive and negative sequence currents and we know the physical impedances of the bank we can readily calculate the neutral unbalance current.

It is worth noting that stem:[|K_1|=|K_2|] are small numbers as for element failures only small changes in capacitive reactance occur. Additionally, stem:[|I_2|] is small for a single element failure. 

What is of importance to us, is to calculate the stem:[K_1] factor as this allows us to relate the neutral current, positive and negative sequence current with the physical characteristics of the capacitor bank.

With an eye to calculate stem:[K_1], we take eq. 20 and multiply by stem:[I_1^*] and we also multiply the _conjugate_ of eq. 20 by stem:[I_2^*]:

[latexmath]
++++
I_1^* I_N = I_1^* (K_1 I_1  + K_1^* I_2  ) \\
I_2 I_N^* = I_2 (K_1^* I_1^*  + K_1 I_2^*  )
++++

Subtracting these two equations from each other gives:

[latexmath]
++++
I_1^* I_N - I_2 I_N^*= I_1^* (K_1 I_1  + K_1^* I_2  ) - I_2 (K_1^* I_1^*  + K_1 I_2^*) \\

I_1^* I_N - I_2 I_N^*= K_1 I_1^*I_1  + K_1^* I_1^* I_2 - K_1^* I_1^* I_2 - K_1 I_2 I_2^* \\

I_1^* I_N - I_2 I_N^*= K_1 I_1^*I_1 - K_1 I_2 I_2^* \\
++++

We use standard proof regarding conjugation, "`The product of a complex number with its conjugate is equal to the square of the number's modulus.`" to yield:

[latexmath]
++++
I_1^* I_N - I_2 I_N^*= K_1 |I_1^2| - K_1 |I_2^2|
++++

Solving now for stem:[K_1]:

[latexmath]
++++
K_1 = \frac{I_1^* I_N - I_2 I_N^*}{|I_1^2| - |I_2^2|}
++++

